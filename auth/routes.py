from flask import Blueprint, render_template, g, request, redirect, session,make_response
from auth import authentification

authentifier = authentification.Auth()

auth = Blueprint('auth', __name__, template_folder='templates', url_prefix='/auth')



@auth.route('/info')
def info():
    return render_template("infosCompte.html", user = g.user)


@auth.route('/loginForm', methods=['POST', 'GET'])
def login():
    if not g.user['name'] == 'Guest':
        return redirect('/auth/info')

    error = False
    if request.method == "POST":
        error = authentifier.se_connecter(request)
        if len(error) == 0:
            resp = make_response(redirect('/'))
            resp.set_cookie('username', request.form['username'])
            return resp
    return render_template('login.html', user=g.user, error=error)


@auth.route('/createAccount', methods=['POST', 'GET'])
def create_account():
    error = False
    if request.method == "POST":
        error = authentifier.creer_compte(request)
        if len(error) == 0:
            resp = make_response(redirect('/'))
            resp.set_cookie('username', request.form['email'])
            return resp
    return render_template("createAccount.html", user=g.user, error=error)


@auth.route('/disconnect')
def disconnect():
    resp = make_response(redirect('/'))
    if request.cookies.get('username'):
        resp.set_cookie('username', "", max_age=0)
    nom = session['username']
    session.pop('username', None)
    session['m-disconnect'] = f"Déconnection éffectuée avec succès. Hâte de vous revoir {nom} :) "
    return resp
