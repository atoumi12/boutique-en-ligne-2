from flask import g, session, redirect
import hashlib


class Auth:

    def se_connecter(self, formulaire):

        user = formulaire.form['username']
        password = formulaire.form['password'].encode('utf-8')
        hash_password = hashlib.md5(password).hexdigest().upper()

        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""
                        Select password
                        from users u
                        Where u.username = %s                
                    """
            params = (user,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()

            error = ""
            if len(records) != 1:
                error = 'Erreur de connection, veuillez vérifier vos informations!'
            else:
                if hash_password == records[0]['password']:
                    session['username'] = user
                    session['messageConnexion'] = f"Connexion éffectuée avec succès! Bon retour {user}"

                    session["info"] = self.obtenir_user(user)

                    return redirect('/auth/loginForm')
                else:
                    error = 'Erreur de connection, veuillez vérifier vos informations!'
        except Exception as e:
            error = f"Error reading data from MySQL table {e}"
        finally:
            return error

    def creer_compte(self, formulaire):

        user = formulaire.form['email']
        password_a = formulaire.form['passwordA'].encode('utf-8')
        password_b = formulaire.form['passwordB'].encode('utf-8')

        error = ""
        if len(user) == 0:
            error = 'Utilisateur vide'
        else:
            try:
                cursor = g.connection.cursor(dictionary=True)
                sql_select_query = f"""
                            Select u.username
                            from users u
                            Where u.username = %s                
                        """
                params = (user,)
                cursor.execute(sql_select_query, params)
                records = cursor.fetchall()
                cursor.close()

            except Exception as e:
                error = f"Error reading data from MySQL table {e}"
            if len(records) == 1:
                error = 'Utilisateur existant, veuillez en choisir un autre '
            else:
                if len(password_a) < 1 or len(password_b) < 1:
                    error = 'Le champs de mot de passe ne doit pas être vide'
                else:
                    if password_a != password_b:
                        error = 'Les deux mots de passe ne sont pas identiques'
                    else:
                        try:
                            # Ajout du customer
                            new_customer = self.add_customer(formulaire)

                            # Ajout du user
                            # id du user
                            cust_id = new_customer["customerNumber"]

                            hash_password = hashlib.md5(password_a).hexdigest().upper()
                            cursor = g.connection.cursor(dictionary=True)
                            sql_select_query = f"""
                                        INSERT INTO users(username, password, customer_id, employee_id) 
                                         VALUES(%s, %s, %s , null );
                                    """
                            params = (user, hash_password, cust_id,)
                            cursor.execute(sql_select_query, params)
                            cursor.close()

                            session['username'] = user
                            session['messageCreationCompte'] = f"Le compte {user} a bien été créé."

                            session["info"] = self.obtenir_user(user)

                        except Exception as e:
                            error = f"Error reading data from MySQL table {e}"

        return error

    def add_customer(self, formulaire):
        try:
            cursor = g.connection.cursor(dictionary=True)
            requete = f""" insert into classicmodels.customers
                                        (   customerNumber,
                                             customerName,
                                             contactLastName,
                                             contactFirstName,
                                             phone,
                                             addressLine1,
                                             addressLine2,
                                             city,
                                             state,
                                             postalCode,
                                             country
                                        ) VALUES (%s, %s,%s, %s, %s , %s, %s, %s, %s, %s, %s)
                            """

            nouveau_id = self.obtenir_dernierCustomerNumber()["customerNumber"] + 1
            # Pour l'instant Le customerName est l'union du nom et prenom
            c_name = f"{formulaire.form['nom'].capitalize()} {formulaire.form['prenom'].capitalize()}"

            adresse_ligne2 = ""
            if len(formulaire.form['adresseB']) > 0:
                adresse_ligne2 = formulaire.form['adresseB']
            else:
                adresse_ligne2 = None

            code_postal = ""
            if len(formulaire.form['codePostal']) > 0:
                code_postal = formulaire.form['codePostal']
            else:
                code_postal = None

            params = (nouveau_id, c_name,
                      formulaire.form['nom'], formulaire.form['prenom'],
                      formulaire.form['phone'], formulaire.form['adresseA'], adresse_ligne2,
                      formulaire.form['ville'], formulaire.form.get('etat', ""),
                      code_postal, formulaire.form['country'],)

            cursor.execute(requete, params)
            g.connection.commit()



            return self.obtenir_customer_avecID(nouveau_id)
        except Exception as e:
            print("Error reading data from MySQL table", e)
            return False

    def obtenir_dernierCustomerNumber(self):

        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""select
                            customerNumber
                            from customers order
                            by
                            customerNumber
                            desc               
                            """

            cursor.execute(sql_select_query)
            records = cursor.fetchall()
            cursor.close()
            return records[0]

        except Exception as e:
            return f"Error reading data from MySQL table {e}"

    def obtenir_customer_avecID(self, son_id):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""select *
                            from customers where 
                            customerNumber = %s               
                            """
            params = (son_id,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()
            return records[0]

        except Exception as e:
            return f"Error reading data from MySQL table {e}"

    def obtenir_user(self, username):
        try:
            cursor = g.connection.cursor(dictionary=True)
            sql_select_query = f"""select *
                            from users where 
                            username = %s               
                            """
            params = (username,)
            cursor.execute(sql_select_query, params)
            records = cursor.fetchall()
            cursor.close()
            return records[0]

        except Exception as e:
            return f"Error reading data from MySQL table {e}"
