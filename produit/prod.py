from flask import g


class Produit:

    def valider_produit(self, request):
        errors = {}

        if "productCode" not in request.form:
            errors['productCode'] = "Pas de code produit."

        if "productName" not in request.form:
            errors['productName'] = "Pas de nom de produit"

        else:
            if len(request.form['productName']) < 3:
                errors['productName'] = "Le nom du produit doit avoir au moins 3 caractères"

            if len(request.form['productName']) > 50:
                errors['productName'] = "Le nom du produit ne doit pas avoir plus de 50 caractères"

        if "productLine" not in request.form:
            errors['productLine'] = "Pas de ligne produit."

        if "productVendor" not in request.form:
            errors['productVendor'] = "Pas de Vendeur de produit."

        if "productScale" not in request.form:
            errors['productScale'] = "Pas de taille de produit."

        if "productDescription" in request.form:
            if len(request.form['productDescription']) < 3:
                errors['productDescription'] = "La description du produit doit avoir au moins 3 caractères"

            if len(request.form['productDescription']) > 200:
                errors['productDescription'] = "La description du produit ne doit pas avoir plus de 300 caractères"
        else:
            errors['productDescription'] = "Pas de description de produit"

        if "MSRP" in request.form:
            try:
                val = float(request.form['MSRP'].replace(',', '.'))
                if val < 0:
                    errors['MSRP'] = "Le prix de vente doit être un entier plus grand que 0"
            except ValueError:
                errors['MSRP'] = "Le prix de vente n'est pas numérique"
        else:
            errors['MSRP'] = "Pas de prix de vente"

        if "quantityInStock" in request.form:
            try:
                val = int(request.form['quantityInStock'])
            except ValueError:
                errors['quantityInStock'] = "Les quantités doivent être un entier"

        if len(list(errors.keys())) > 0:
            return errors
        else:
            return False

    def one_produit(self, productCode):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)

            requete = f"""select * from products where productCode = %s ;"""

            params = (productCode,)
            cursor.execute(requete, params)

            # get all records
            records = cursor.fetchall()
            return records[0]

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def update_produit(self, formulaire):

        ##validation
        errors =self.valider_produit(formulaire)

        if errors:
            return errors
        else:

            try:
                connection = g.connection
                cursor = connection.cursor(dictionary=True)

                requete = f"""update products
                            set productName = %s,
                                productLine = %s,
                                productScale = %s,
                                productVendor = %s,
                                productDescription =%s ,
                                quantityInStock = %s,
                                MSRP = %s
                            where productCode = %s
                """
                params = (formulaire.form['productName'], formulaire.form['productLine'],
                          formulaire.form['productScale'], formulaire.form['productVendor'],
                          formulaire.form['productDescription'], formulaire.form['quantityInStock'],
                          formulaire.form['MSRP'], formulaire.form['productCode'],)

                cursor.execute(requete, params)
                # commit
                connection.commit()
                return False

            except Exception as e:
                print("Error reading data from MySQL table", e)

    def add_produit(self, formulaire):

        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)
            requete = f""" insert into products

                               (   productCode,
                                   productName ,
                                   productLine,
                                   productScale,
                                   productVendor,
                                   productDescription,
                                   quantityInStock,
                                   buyPrice,
                                   MSRP)
                               values (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                   """
            params = (formulaire.form['productCode'], formulaire.form['productName'],
                      formulaire.form['productLine'], formulaire.form['productScale'],
                      formulaire.form['productVendor'], formulaire.form['productDescription'],
                      formulaire.form['quantityInStock'], formulaire.form['MSRP'], formulaire.form['MSRP'],)
            cursor.execute(requete, params)
            new_product_code = formulaire.form['productCode']
            # commit
            connection.commit()
            return new_product_code
        except Exception as e:
            print("Error reading data from MySQL table", e)
            return False

    def supprimer_produit(self,productCode):
        try:
            connection = g.connection()
            cursor = connection.cursor(dictionary=True)
            requete = f""" delete  from products
                            where productCode = %s ; """

            params = (productCode,)
            cursor.execute(requete, params)

            connection.commit()

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def products_scales(self):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)
            requete = f"""select distinct productScale from products;"""
            cursor.execute(requete)
            # get all records
            records = cursor.fetchall()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def products_vendors(self):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)
            requete = f"""select distinct productVendor from products;"""
            cursor.execute(requete)
            # get all records
            records = cursor.fetchall()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def produit_vide(self):
        return {
            'productName': "",
            'productLine': "",
            'productScale': -1,
            'productVendor': "",
            'productDescription': "",
            'quantityInStock': -1,
            'MSRP': -1
        }


    def recherche_produit(self, keyword='', page=1, sortOrder='False'):
        try:
            connection = g.connection

            element_par_page = 10
            offset = (page - 1) * element_par_page

            requete = f"""select * from products  where productName like %s 
                               or productDescription like %s
                             order by MSRP """

            requete += f"{'asc' if sortOrder == 'False' else 'desc'} limit %s,%s ;"

            cursor = connection.cursor(dictionary=True)

            params = ('%{}%'.format(keyword), '%{}%'.format(keyword), offset, element_par_page,)
            cursor.execute(requete, params)

            # get all records
            records = cursor.fetchall()
            cursor.close()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)


