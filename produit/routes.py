from flask import Blueprint, request, render_template, redirect, url_for, session, g
from produit import prod
from web import mainPage

produit = Blueprint('produit', __name__, template_folder='templates', url_prefix='/produit')

products = prod.Produit()
main_Page = mainPage.HomePage()


@produit.route('/detail/<productCode>')
def index(productCode):
    prods = products.one_produit(productCode)
    if prods is None:
        return redirect('/error/404')

    return render_template('detailProduit.html', produit=prods, user=g.user)


# Modification du produit
@produit.route('/edit/<productCode>', methods=['POST', 'GET'])
def editProduit(productCode):
    if g.user['name'] == 'Guest':
        return redirect('/error/403')
    elif g.user['info']['employee_id'] is None:
        return redirect('/error/403')

    errors = False
    ok = False
    if request.method == "POST":
        errors = products.update_produit(request)
        if not errors:
            ok = True

    # erreur si produit inexistant
    produit_a_modifier = products.one_produit(productCode)
    if produit_a_modifier is None:
        return redirect('/error/404')

    return render_template("actionProduit.html", user=g.user, produit=products.one_produit(productCode),
                           lines=main_Page.prods_lines(),
                           vendors=products.products_vendors(),
                           scales=products.products_scales(), ok=ok, errors=errors)


# Ajout d'un produit
@produit.route('/add', methods=['POST', 'GET'])
def add_produit():
    if g.user['name'] == 'Guest':
        return redirect('/error/403')
    elif g.user['info']['employee_id'] is None:
        return redirect('/error/403')

    errors = False
    ok = False
    if request.method == "POST":
        new_productCode = products.add_produit(request)
        if not new_productCode:
            return "Une erreur s'est produite."

        return redirect(url_for('produit.index', productCode=new_productCode))

    return render_template("actionProduit.html", user=g.user, produit=products.produit_vide(),
                           lines=main_Page.prods_lines(),
                           vendors=products.products_vendors(),
                           scales=products.products_scales(), ok=ok, errors=errors)


# Suppression du produit
@produit.route('/delete/<productCode>')
def deleteProduit(productCode):
    if g.user['name'] == 'Guest':
        return redirect('/error/403')
    elif g.user['info']['employee_id'] is None:
        return redirect('/error/403')

    products.supprimer_produit(productCode)
    session["messageSuppProd"] = "Le produit a été supprimé avec succès"
    return redirect('/')


# Recherche d'un produit
@produit.route('/recherche', methods=['POST', 'GET'])
def recherche():
    # methods = ['POST', 'GET']
    # sort_order = request.args.get('sortOrder', default='False', type=str)
    # page = request.args.get('page', default=1, type=int)
    # if page == 0:
    #     page = 1
    #
    # key_word = ""
    # if request.method == "POST":
    #     key_word = request.form['keyword']
    # # resultat_recherche = products.recherche_produit(key_word, page, sort_order)
    # recherche = resultat_recherche,
    # keyword = key_word, page = page,
    # sortOrder = 'False' if sort_order == 'True' else 'True'
    key_word = ""
    if request.method == "POST":
        key_word = request.form['keyword']
    return render_template('recherche.html', user=g.user, keyword=key_word)
