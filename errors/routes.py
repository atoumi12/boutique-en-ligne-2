from flask import Blueprint,abort


errors = Blueprint('erros', __name__, url_prefix='/error')


@errors.route('/403')
def error403():
    abort(403)

@errors.route('404')
def error404():
    abort(404)