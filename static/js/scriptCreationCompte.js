"use strict";


var creerCompte = {
    "regexCourriel": /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    "courriel": null,
    "erreur-email": null,

    "regexPhone": /^[1-9]\d{2}-\d{3}-\d{4}/,
    "phone": null,
    "erreur-phone": null,


    "nom": null,
    "erreur-nom": null,

    "prenom": null,
    "erreur-prenom": null,

    "passwordA": null,
    "erreur-passwordA": null,
    "passwordB": null,
    "erreur-passwordB": null,

    "adresseA": null,
    "erreur-adresseA": null,
    "adresseB": null,
    "erreur-adresseB": null,

    "ville": null,
    "erreur-ville": null,


    "verificationNom": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-nom"].textContent = "";

        creerCompte.nom.value = creerCompte.nom.value.trim();
        var msgErreur = "";
        if (creerCompte.nom.value.length < 3) {
            msgErreur = "Le nom doit avoir au moins 3 caractères.";
        }

        if (creerCompte.nom.value.length > 50) {
            msgErreur = "Le nom ne doit pas dépasser 50 caratères.";
        }

        creerCompte["erreur-nom"].appendChild(document.createTextNode(msgErreur));
    },

    "verificationPrenom": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-prenom"].textContent = "";

        creerCompte.prenom.value = creerCompte.prenom.value.trim();
        var msgErreur = "";
        if (creerCompte.prenom.value.length < 3) {
            msgErreur = "Le prénom doit avoir au moins 3 caractères.";
        }

        if (creerCompte.prenom.value.length > 50) {
            msgErreur = "Le prénom ne doit pas dépasser 50 caratères.";
        }

        creerCompte["erreur-prenom"].appendChild(document.createTextNode(msgErreur));

    },

    "verificationCourriel": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-email"].textContent = "";

        creerCompte.courriel.value = creerCompte.courriel.value.trim();
        var msgErreur = "";
        if (!creerCompte.regexCourriel.test(creerCompte.courriel.value)) {
            msgErreur = "Le courriel doit être valide.";
        }

        if (creerCompte.courriel.value.length > 50) {
            msgErreur = "Le courriel ne doit pas dépasser 50 caratères.";
        }

        creerCompte["erreur-email"].appendChild(document.createTextNode(msgErreur));

    },

    "verificationPhone": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-phone"].textContent = "";

        creerCompte.phone.value = creerCompte.phone.value.trim();
        var msgErreur = "";
        if (!creerCompte.regexPhone.test(creerCompte.phone.value)) {
            msgErreur = "Veuillez respecter le format  ( ###-###-#### )";
        }
        creerCompte["erreur-phone"].appendChild(document.createTextNode(msgErreur));


    },

    "verificationPasswordA": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-passwordA"].textContent = "";

        creerCompte.passwordA.value = creerCompte.passwordA.value.trim();
        var msgErreur = "";
        if (creerCompte.passwordA.value.length < 4) {
            msgErreur = "Le mot de passe doit contenir au moins 4 caratères.";
        }

        creerCompte["erreur-passwordA"].appendChild(document.createTextNode(msgErreur));


    },

    "verificationPasswordB": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-passwordB"].textContent = "";

        creerCompte.passwordB.value = creerCompte.passwordB.value.trim();
        var msgErreur = "";
        if (creerCompte.passwordB.value != creerCompte.passwordA.value.trim()) {
            msgErreur = "Les deux mots de passe doivent être identiques.";
        }

        creerCompte["erreur-passwordB"].appendChild(document.createTextNode(msgErreur));

    },

    "verificationAdresseA": function () {

        creerCompte["erreur-adresseA"].textContent = '';

        creerCompte.adresseA.value = creerCompte.adresseA.value.trim();
        var msgErreur = "";
        if (creerCompte.adresseA.value.length < 3) {
            msgErreur = "L'adresse ligne 1 doit avoir au moins 3 caractères.";
        }

        if (creerCompte.adresseA.value.length > 50) {
            msgErreur = "L'adresse ligne 1 ne doit pas dépasser 50 caratères.";
        }

        creerCompte["erreur-adresseA"].appendChild(document.createTextNode(msgErreur));

    },

    "verificationAdresseB": function () {

        creerCompte["erreur-adresseB"].textContent = "";

        creerCompte.adresseB.value = creerCompte.adresseB.value.trim();

        if (creerCompte.adresseB.value.length >= 1) {

            var msgErreur = "";
            if (creerCompte.adresseB.value.length < 3) {
                msgErreur = "L'adresse ligne 2 doit avoir au moins 3 caractères.";
            }

            if (creerCompte.adresseB.value.length > 50) {
                msgErreur = "L'adresse ligne 2 ne doit pas dépasser 50 caratères.";
            }

            creerCompte["erreur-adresseB"].appendChild(document.createTextNode(msgErreur));
        }

    },

    "verificationVille": function () {

        /* s'assurer de vider l'erreur */
        creerCompte["erreur-ville"].textContent = "";
        creerCompte.ville.value = creerCompte.ville.value.trim();

        var msgErreur = "";
        if (creerCompte.ville.value.length < 3) {
            msgErreur = "Le nom de la ville doit avoir au moins 3 caractères.";
        }

        if (creerCompte.ville.value.length > 50) {
            msgErreur = "Le nom de la ville ne doit pas dépasser 50 caratères.";
        }

        creerCompte["erreur-ville"].appendChild(document.createTextNode(msgErreur));
    },

    "viderChampsErreur": function () {

        creerCompte["erreur-nom"].textContent = "";
        creerCompte["erreur-prenom"].textContent = "";
        creerCompte["erreur-email"].textContent = "";
        creerCompte["erreur-phone"].textContent = "";
        creerCompte["erreur-adresseA"].textContent = "";
        creerCompte["erreur-passwordA"].textContent = '';
        creerCompte["erreur-passwordB"].textContent = "";
        creerCompte["erreur-adresseA"].textContent = "";
        creerCompte["erreur-adresseB"].textContent = "";
        creerCompte["erreur-ville"].textContent = "";
    },

    "trimLesChamps": function () {

        creerCompte.nom.value = creerCompte.nom.value.trim();
        creerCompte.prenom.value = creerCompte.prenom.value.trim();

        creerCompte.courriel.value = creerCompte.courriel.value.trim();
        creerCompte.phone.value = creerCompte.phone.value.trim();

        creerCompte.passwordA.value = creerCompte.passwordA.value.trim();
        creerCompte.passwordB.value = creerCompte.passwordB.value.trim();

        creerCompte.adresseA.value = creerCompte.adresseA.value.trim();
        creerCompte.adresseB.value = creerCompte.adresseB.value.trim();

    },

    "gererSubmit": function (evenement) {

        creerCompte.viderChampsErreur();
        creerCompte.trimLesChamps();

        if (creerCompte.nom.value.length < 3) {
            creerCompte["erreur-nom"].appendChild(document.createTextNode("Le nom doit avoir au moins 3 caractères."));
            evenement.preventDefault();
        } else if (creerCompte.nom.value.length > 50) {
            creerCompte["erreur-nom"].appendChild(document.createTextNode("Le nom ne doit pas dépasser 50 caratères."));
            evenement.preventDefault();
        }

        if (creerCompte.prenom.value.length < 3) {
            creerCompte["erreur-prenom"].appendChild(document.createTextNode("Le prénom doit avoir au moins 3 caractères."));
            evenement.preventDefault();
        } else if (creerCompte.prenom.value.length > 50) {
            creerCompte["erreur-prenom"].appendChild(document.createTextNode("Le prénom ne doit pas dépasser 50 caratères."));
            evenement.preventDefault();
        }

        if (creerCompte.courriel.value.length > 50) {
            creerCompte["erreur-email"].appendChild(document.createTextNode("Le courriel ne doit pas dépasser 50 caratères."));
            evenement.preventDefault();
        } else if (!creerCompte.regexCourriel.test(creerCompte.courriel.value)) {
            creerCompte["erreur-email"].appendChild(document.createTextNode("Le courriel doit être valide."));
            evenement.preventDefault();
        }

        if (!creerCompte.regexPhone.test(creerCompte.phone.value)) {
            creerCompte["erreur-phone"].appendChild(document.createTextNode("Veuillez respecter le format  ( ###-###-#### )"));
            evenement.preventDefault();
        }

        if (creerCompte.passwordA.value.length < 4) {
            creerCompte["erreur-passwordA"].appendChild(document.createTextNode("Le mot de passe doit contenir au moins 4 caratères."));
            evenement.preventDefault();
        }

        if (creerCompte.passwordB.value != creerCompte.passwordA.value) {
            creerCompte["erreur-passwordB"].appendChild(document.createTextNode("Les deux mots de passe doivent être identiques."));
            evenement.preventDefault();
        }

        if (creerCompte.adresseA.value.length < 3) {
            creerCompte["erreur-adresseA"].appendChild(document.createTextNode("L'adresse ligne 1 doit avoir au moins 3 caractères."));
            evenement.preventDefault();
        } else if (creerCompte.adresseA.value.length > 50) {
            creerCompte["erreur-adresseA"].appendChild(document.createTextNode("L'adresse ligne 1 ne doit pas dépasser 50 caratères."));
            evenement.preventDefault();
        }

        if (creerCompte.adresseB.value.length >= 1) {

            if (creerCompte.adresseB.value.length < 3) {
                creerCompte["erreur-adresseB"].appendChild(document.createTextNode("L'adresse ligne 2 doit avoir au moins 3 caractères."));
                evenement.preventDefault();
            } else if (creerCompte.adresseB.value.length > 50) {
                creerCompte["erreur-adresseB"].appendChild(document.createTextNode("L'adresse ligne 2 ne doit pas dépasser 50 caratères."));
                evenement.preventDefault();
            }

        }

        if (creerCompte.ville.value.length < 3) {
            creerCompte["erreur-ville"].appendChild(document.createTextNode("Le nom de la ville doit avoir au moins 3 caractères."));
            evenement.preventDefault();
        } else if (creerCompte.ville.value.length > 50) {
            creerCompte["erreur-ville"].appendChild(document.createTextNode("Le nom de la ville ne doit pas dépasser 50 caratères."));
            evenement.preventDefault();
        }

        // Vu le choix d'etat n'est pas obligatoire, je genere le country automatiquement
        // si l'utilisateur ne choisit pas d'etat pour que la requete SQL fonctionne
        // parce que le champs country est obligatoire dans la table customers :)
        if (document.getElementById('country').value.length < 1){
            document.getElementById('country').value = 'Canada';
        }

    },

    "gererPays": function () {

        var etat = document.getElementById('etat');
        var country = ""
        if (etat.value == 'ca') {
            country = "Canada"
        } else if (etat.value == 'usa') {
            country = "États-Unis"
        }
        var champsCoutry = document.getElementById('country');
        champsCoutry.value = country;

    },


    "initialisation": function () {

        /* Nom */
        creerCompte.nom = document.getElementById('nom');
        creerCompte["erreur-nom"] = document.getElementById('msg-nom');
        creerCompte.nom.addEventListener("keyup", creerCompte.verificationNom, false);

        /* Prenom */
        creerCompte.prenom = document.getElementById('prenom');
        creerCompte["erreur-prenom"] = document.getElementById('msg-prenom');
        creerCompte.prenom.addEventListener('keyup', creerCompte.verificationPrenom, false);

        /* Courriel */
        creerCompte.courriel = document.getElementById('email');
        creerCompte["erreur-email"] = document.getElementById('msg-email');
        creerCompte.courriel.addEventListener("keyup", creerCompte.verificationCourriel, false);

        /* Phone number*/
        creerCompte.phone = document.getElementById('phone');
        creerCompte["erreur-phone"] = document.getElementById('msg-phone');
        creerCompte.phone.addEventListener('keyup', creerCompte.verificationPhone, false);

        /* Les mots de passe */
        creerCompte.passwordA = document.getElementById('passwordA');
        creerCompte["erreur-passwordA"] = document.getElementById('msg-passwordA');
        creerCompte.passwordA.addEventListener('keyup', creerCompte.verificationPasswordA, false);

        creerCompte.passwordB = document.getElementById('passwordB');
        creerCompte["erreur-passwordB"] = document.getElementById('msg-passwordB');
        creerCompte.passwordB.addEventListener('change', creerCompte.verificationPasswordB, false);

        /* Les adresses */
        creerCompte.adresseA = document.getElementById('adresseA');
        creerCompte["erreur-adresseA"] = document.getElementById('msg-adresseA');
        creerCompte.adresseA.addEventListener('keyup', creerCompte.verificationAdresseA, false);

        creerCompte.adresseB = document.getElementById('adresseB');
        creerCompte["erreur-adresseB"] = document.getElementById('msg-adresseB');
        creerCompte.adresseB.addEventListener('keyup', creerCompte.verificationAdresseB, false);

        /* La ville */
        creerCompte.ville = document.getElementById('ville');
        creerCompte["erreur-ville"] = document.getElementById('msg-ville');
        creerCompte.ville.addEventListener('keyup', creerCompte.verificationVille, false);

        /* Le pays */
        var etat = document.getElementById('etat');
        etat.addEventListener('change', creerCompte.gererPays, false);

        /* soumettre le formulaire */
        document.getElementById('formulaire').addEventListener('submit', creerCompte.gererSubmit, false);


    },


};


window.addEventListener("load", creerCompte.initialisation, false);
