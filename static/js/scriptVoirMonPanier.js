"use strict";


var voirPanier = {
    "div-tableau": null,
    "tableau": null,
    "form": null,
    "username": document.cookie.split('=')[1],
    "panier": JSON.parse(localStorage.getItem(document.cookie.split('=')[1])),
    "total": null,


    "CreationPanier": function () {

        if (voirPanier.panier != null && voirPanier.panier.length > 0) {

            $("#div-pay").removeClass("cacher")

            var titre = document.createElement('h4');
            titre.appendChild(document.createTextNode("Panier "));
            titre.style.marginBottom = "100px";
            voirPanier.tableau.appendChild((titre));

            // Premire ligne du tableau
            voirPanier.ajouterLigne("Image du produit", "Nom du produit", "Prix", "Quantité disponible",
                "Nombre d'articles", "Vendeur", 'th')

            for (var i = 0; i < voirPanier.panier.length; i++) {
                var article = voirPanier.panier[i];

                // image
                var image = document.createElement('img');
                image.src = article.img;
                image.style.width = "100px";
                image.style.borderRadius = "10px";

                // Nom du produit avec un lien vers la page de detail
                var nom = document.createElement('a');
                nom.appendChild(document.createTextNode(article.nom));
                nom.href = `/produit/detail/${article.code}`;

                // Nombre d'articles
                var input = document.createElement('input');
                input.type = 'number';
                input.value = article.nb;
                input.min = 0;

                input.id = `${article.qte}-${article.code}`;
                input.setAttribute('nom-p', `${article.nom}`);

                input.addEventListener('keyup', voirPanier.validationNbArticles, false);
                input.addEventListener('change', voirPanier.nouveauNbArticles, false);
                input.addEventListener('change', voirPanier.calculTotal, false)


                voirPanier.ajouterLigne(image, nom, `${article.prix}$`, article.qte, input, article.vendeur);
            }


        } else {

            var message = document.createElement('h4');
            message.appendChild(document.createTextNode(("Votre panier est vide, Veuillez y ajouter des articles pour y accéder :) ")));
            voirPanier["div-tableau"].appendChild(message);
        }


    },

    "calculTotal": function () {
        $('#total-commande').empty()

        if (voirPanier.panier != null) {
            voirPanier.total = 0

            for (var i = 0; i < voirPanier.panier.length; i++) {
                var article = voirPanier.panier[i];

                var somme_article = parseFloat(article.nb) * parseFloat(article.prix)

                voirPanier.total += somme_article
            }

            document.getElementById("total-commande").appendChild(document.createTextNode(`${parseFloat(voirPanier.total).toFixed(2)} $`))

        }

    },
    "ajouterCell": function (ligne, contenuCellule, typeCellule = 'td') {

        var cell = document.createElement(typeCellule);
        cell.append(contenuCellule);
        ligne.appendChild(cell);

    },

    "ajouterLigne": function (img, nom, prix, qte, nb, vendeur, typeCellule = 'td') {

        var ligne = document.createElement('tr');

        voirPanier.ajouterCell(ligne, img, typeCellule);
        voirPanier.ajouterCell(ligne, nom, typeCellule);
        voirPanier.ajouterCell(ligne, prix, typeCellule);
        voirPanier.ajouterCell(ligne, qte, typeCellule);
        voirPanier.ajouterCell(ligne, nb, typeCellule);
        voirPanier.ajouterCell(ligne, vendeur, typeCellule);


        voirPanier.tableau.appendChild(ligne);


    },

    // 2 methodes pour valider et changer le nombre d'articles du clients
    "validationNbArticles": function (evenement) {

        var id = evenement.currentTarget.id;
        var td = evenement.currentTarget.parentElement;

        // Quantité disponible de l'article
        var qte = parseInt(id.split('-')[0]);
        var valeur = parseInt(evenement.currentTarget.value);

        // Vider le span d'erreur
        var erreur = document.getElementById(`erreur-${id.split('-')[1]}`);
        if (erreur != null) {
            td.removeChild(erreur);
        }


        if (valeur > qte) {

            var span = document.createElement('span');
            span.id = `erreur-${id.split('-')[1]}`;

            span.className = 'erreur';

            span.appendChild(document.createTextNode(`La quantité maximale est : ${qte} `));
            td.appendChild(span);
        }


    },

    "nouveauNbArticles": function (event) {

        var input = event.currentTarget;
        var nb_articles = input.value;
        var nom_produit = input.getAttribute('nom-p');

        if (voirPanier.panier != null) {

            for (var i = 0; i < voirPanier.panier.length; i++) {
                var article = voirPanier.panier[i];

                if (article.nom.trim() == nom_produit.trim()) {
                    if (nb_articles > 0 && nb_articles < article.qte) {
                        voirPanier.panier[i].nb = nb_articles;
                    }
                }
            }
        }

        localStorage.setItem(voirPanier.username, JSON.stringify(voirPanier.panier));

    },


    "viderInputsFields": function () {

        if (voirPanier.panier != null && voirPanier.panier.length > 0) {

            for (var i = 0; i < voirPanier.panier.length; i++) {
                var article = voirPanier.panier[i];

                var inputField = document.getElementById(`${article.qte}-${article.code}`);
                var td = inputField.parentElement;
                var erreur = document.getElementById(`erreur-${inputField.id.split('-')[1]}`);

                if (erreur != null && erreur.textContent.length > 0) {
                    td.removeChild(erreur);
                }
            }


        }

    },

    "gererSubmitForm": function (event) {

        voirPanier.viderInputsFields();


        var username = document.cookie.split('=')[1];
        var panier = JSON.parse(localStorage.getItem(username));


        if (panier != null && panier.length > 0) {

            for (var i = 0; i < panier.length; i++) {

                var article = panier[i];
                var inputField = document.getElementById(`${article.qte}-${article.code}`);
                var td = inputField.parentElement;

                if (parseInt(inputField.value) > parseInt(article.qte)) {

                    var span = document.createElement('span');
                    span.id = `erreur-${article.code}`;

                    span.className = 'erreur';

                    span.appendChild(document.createTextNode(`La quantité maximale est : ${article.qte} `));
                    td.appendChild(span);


                    //Submit
                    event.preventDefault();


                }
            }

        }

        voirPanier.envoyerCommande();
    },


    "envoyerCommande": function () {

        var le_panier = JSON.parse(localStorage.getItem(voirPanier.username));

        var le_user = voirPanier.username.toString().trim();
        var date_achat = Date.now();


        if (localStorage.getItem('historique') != null) {

            var historique_complet = localStorage.getItem('historique');


            if (le_user in Object.keys(historique_complet)) {

                le_user = le_user.substr(1, le_user.length - 2);

                historique_complet[le_user].panier.push(le_panier);
                historique_complet[le_user].date.push(date_achat);

            } else {

                le_user = le_user.substr(1, le_user.length - 2);

                historique_complet[le_user] = {};
                historique_complet[le_user] = {
                    "panier": [le_panier],
                    "date": [date_achat]
                };


            }

            localStorage.setItem('historique', JSON.stringify(historique_complet));

        } else {

            var nv_historique = {};

            le_user = le_user.substr(1, le_user.length - 2);

            nv_historique[le_user] = {}
            nv_historique[le_user] = {

                'panier': [le_panier],
                'date': [date_achat]
            }


            localStorage.setItem('historique', JSON.stringify(nv_historique));
        }

        le_user = le_user.substr(1, le_user.length - 2);
        localStorage.removeItem(le_user);
    },

    "initialisation": function () {

        voirPanier["div-tableau"] = document.getElementById('div-tableau');

        voirPanier.form = document.getElementById('formPanier');
        voirPanier.form.addEventListener('submit', voirPanier.gererSubmitForm, false);

        voirPanier.tableau = document.getElementById('tableau');

        voirPanier.CreationPanier();

        voirPanier.calculTotal();


    },


}


window.addEventListener("load", voirPanier.initialisation, false);