"use strict";


var scriptPanier = {
    "username":null,
    "div-panier": null,
    "bouton": null,
    "panierAchat": null,


    "gererClickBouton": function () {
        scriptPanier.username = document.cookie.split('=')[1];
        //var user_name = document.cookie.split('=')[1];
        var t = localStorage.getItem(scriptPanier.username);
        if (t == null || t.length<0) {
            scriptPanier.AfficherpanierVide();
        } else {
            scriptPanier.panierAchat = JSON.parse(t);
            scriptPanier.AfficherPanierRempli();
        }

        setTimeout(scriptPanier.DOMDetectionClickAilleurs, 1000);
    },

    "gererClickEnDehorsPanier": function (evenement) {

        var clicDedans = scriptPanier["div-panier"].contains(evenement.target);
        if (!clicDedans) {
            scriptPanier.viderElements(scriptPanier["div-panier"]);
            scriptPanier["div-panier"].className = "cacher affichage-p";
            document.removeEventListener('click', scriptPanier.gererClickEnDehorsPanier);
        }
    },

    "viderElements": function (element) {

        while (element.lastChild) {
            element.removeChild(element.lastChild);
        }
    },

    "AfficherpanierVide": function () {

        scriptPanier.viderElements(scriptPanier["div-panier"]);
        scriptPanier["div-panier"].className = "montrer affichage-p";

        var titre = document.createElement('h6');
        titre.appendChild(document.createTextNode("Details du panier d'achats :"));
        titre.style.marginBottom = "15px";
        scriptPanier["div-panier"].appendChild(titre);

        var texte = document.createElement('p')
        texte.appendChild(document.createTextNode("Votre panier est vide"))
        scriptPanier["div-panier"].appendChild(texte);
        scriptPanier["div-panier"].style.margin = "15px";
        scriptPanier["div-panier"].style.padding = "15px";


        scriptPanier["div-panier"].removeChild(document.getElementById('btnVoirPanier'));

    },

    "AfficherPanierRempli": function () {

        scriptPanier.viderElements(scriptPanier["div-panier"]);
        scriptPanier["div-panier"].className = "montrer affichage-p";

        var titre = document.createElement('h6');
        titre.appendChild(document.createTextNode("Details du panier d'achats :"));
        titre.style.marginBottom = "15px";
        scriptPanier["div-panier"].appendChild(titre);

        if (scriptPanier.panierAchat.length > 0) {

            for (var i = 0; i < scriptPanier.panierAchat.length; i++) {

                var article = scriptPanier.panierAchat[i];
                scriptPanier.CreerPanier(article);
            }
        }

        scriptPanier["div-panier"].style.margin = "15px";
        scriptPanier["div-panier"].style.padding = "15px";

        var voirPanier = document.createElement('a');
        voirPanier.id = 'btnVoirPanier'
        voirPanier.href = "/panier";
        voirPanier.appendChild(document.createTextNode("Voir mon panier"));
        scriptPanier["div-panier"].appendChild(voirPanier);


    },

    "CreerPanier": function (article) {

        var img = document.createElement('img');
        img.src = article.img;
        img.style.width = "80px";
        img.style.marginRight = "15px";
        img.style.borderRadius = "5px";

        var ligne = document.createElement('p');
        ligne.appendChild(img);
        var lien = document.createElement('a')
        lien.href = `/produit/detail/${article.code}`;
        lien.appendChild(document.createTextNode(article.nom))
        ligne.appendChild(lien);

        var input = document.createElement('input');
        input.type = "number";
        input.min = "0";
        input.className = "nb-p";
        input.value = article.nb;
        input.id = `nb-${article.nom}`;
        input.addEventListener('change', scriptPanier.changementNbArticle, false);
        ligne.appendChild(input);

        var supp = document.createElement('button');
        supp.className = "supp-p";
        supp.id = `supp-${article.nom}`;
        supp.appendChild(document.createTextNode("X"));
        supp.addEventListener('click', scriptPanier.SupprimerArticle, false);
        ligne.appendChild(supp);

        scriptPanier["div-panier"].appendChild(ligne);
    },

    "changementNbArticle": function (evenement) {
        var nomArticle = evenement.currentTarget.id.substr(3);
        var nouveau_nb = evenement.currentTarget.value;

        if (scriptPanier.panierAchat.length > 0) {

            for (var i = 0; i < scriptPanier.panierAchat.length; i++) {

                var article = scriptPanier.panierAchat[i];
                if (article.nom.trim() == nomArticle.trim()) {
                    scriptPanier.panierAchat[i].nb = nouveau_nb;
                }
            }
        }


        //var user = document.cookie.split('=')[1];
        localStorage.setItem(scriptPanier.username, JSON.stringify(scriptPanier.panierAchat));
        scriptPanier.AfficherPanierRempli();


    },

    "SupprimerArticle": function (evenement) {

        var nomArticle = evenement.currentTarget.id.substr(5);

        if (scriptPanier.panierAchat.length > 0) {

            for (var i = 0; i < scriptPanier.panierAchat.length; i++) {

                var article = scriptPanier.panierAchat[i];
                if (article.nom.trim() == nomArticle.trim()) {
                    scriptPanier.panierAchat.pop(i);
                }
            }
        }

        //var user = document.cookie.split('=')[1];
        localStorage.setItem(scriptPanier.username, JSON.stringify(scriptPanier.panierAchat));
        scriptPanier.gererClickBouton();


    },

    "DOMDetectionClickAilleurs": function () {
        document.addEventListener('click', scriptPanier.gererClickEnDehorsPanier, false)
    },


    "initialisation": function () {
        scriptPanier["div-panier"] = document.getElementById('creation-panier');
        scriptPanier.bouton = document.getElementById('btn-panier');
        scriptPanier.bouton.addEventListener('click', scriptPanier.gererClickBouton, false);
    },
};


window.addEventListener("load", scriptPanier.initialisation, false);
