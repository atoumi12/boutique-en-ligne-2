"use strict";


var panier = {
    "btnAjoutPanier": null,

    "produit-code": null,
    "produit_img_url": null,
    "produit_nom": null,

    "produit-prix": null,
    "produit-productLine": null,
    "produit-vendeur": null,
    "produit-qteDispo":null,

    "customer_name": null,

    "gererPanier": function () {


        if (localStorage.getItem(panier.customer_name) != null) {
            var un_panier = localStorage.getItem(panier.customer_name);
            var le_panier = JSON.parse(un_panier);

            var trouver = panier.verifierArticleDansPanier(le_panier, panier.produit_nom);


            if (trouver) {

                let indice;
                for (let i = 0; i < le_panier.length; i++) {
                    if (le_panier[i].nom == panier.produit_nom) {
                        indice = i;
                    }
                }
                le_panier[indice].nb = panier.AjouterDesArticles(le_panier[indice].nb);

            } else {

                le_panier.push({
                    "nom": panier.produit_nom,
                    "img": panier.produit_img_url,
                    "code": panier["produit-code"],
                    "prix": panier["produit-prix"],
                    "qte" :panier["produit-qteDispo"],
                    "productLine": panier["produit-productLine"],
                    "vendeur": panier["produit-vendeur"],
                    "nb": 1
                })
            }

            localStorage.setItem(panier.customer_name, JSON.stringify(le_panier));

        } else {

            var nv_panier = [];
            nv_panier.push({
                "nom": panier.produit_nom,
                "img": panier.produit_img_url,
                "code": panier["produit-code"],
                "prix": panier["produit-prix"],
                "qte" :panier["produit-qteDispo"],
                "productLine": panier["produit-productLine"],
                "vendeur": panier["produit-vendeur"],
                "nb": 1
            });
            localStorage.setItem(panier.customer_name, JSON.stringify(nv_panier));

        }

        alert("Produit ajouté avec succès!");
    },

    "verifierArticleDansPanier": function (panier, article) {
        if (panier != null) {
            for (var i = 0; i < panier.length; i++) {

                if (panier[i].nom == article) {
                    return true;
                }
            }
            return false;
        }
    },

    "AjouterDesArticles": function (element, nb = 1) {
        return element + nb;
    },

    "boutonAjouter": function () {

        var cookies = document.cookie;
        if (cookies.length < 1) {
            panier.btnAjoutPanier.href = "/auth/loginForm";
        } else {
            panier.gererPanier();
        }

    },

    "initialisation": function () {

        /* Informations sur le produit*/
        panier["produit-code"] = document.getElementById('p-code').textContent;
        panier.produit_img_url = document.getElementById('p-img').src;
        panier.produit_nom = document.getElementById('p-name').textContent.substring(5);

        var p = document.getElementById('p-prix').textContent;
        panier["produit-prix"] = p.substr(0, p.length - 1);

        panier["produit-productLine"] = document.getElementById('p-productLine').textContent.trim();
        panier["produit-vendeur"] = document.getElementById('p-vendeur').textContent.trim();
        panier["produit-qteDispo"] = document.getElementById('p-qte').textContent.trim();



        /* Données de l'utilisateur */
        panier.customer_name = document.cookie.split('=')[1];

        panier.btnAjoutPanier = document.getElementById('btn-ajouter-panier');
        panier.btnAjoutPanier.addEventListener('click', panier.boutonAjouter, false);


    },


}


window.addEventListener("load", panier.initialisation, false);