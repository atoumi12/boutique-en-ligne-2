"use strict";


var historiquePanier = {


    "username": document.cookie.split('=')[1],
    "panier": JSON.parse(localStorage.getItem('historique')),
    "div-historique": null,

    "historique": function () {


        if (historiquePanier.panier != null ) {

            historiquePanier.username = historiquePanier.username.substr(1, historiquePanier.username.length - 2);
            historiquePanier.panier = historiquePanier.panier[historiquePanier.username];

            var titre = document.createElement('h4');
            titre.appendChild(document.createTextNode("Voici l'historique de vos achats:"));
            historiquePanier["div-historique"].insertBefore(titre, historiquePanier["table-historique"]);
            var table = document.createElement('table');

            // Les titres
            historiquePanier.ajouterLigne(table, "Image de l'achat", "Nom", "Prix", "Nombre d'articles", 'th');

            for (var i = 0; i < historiquePanier.panier.panier.length; i++) {

                console.log(historiquePanier.panier.panier[i][i]);
                var commande = historiquePanier.panier.panier[i][i];

                // image
                var image = document.createElement('img');
                image.src = commande.img;
                image.style.width = "100px";
                image.style.borderRadius = "10px";
                image.id = `${commande.code}`;

                // Nom du produit avec un lien vers la page de detail
                var nom = document.createElement('a');
                nom.appendChild(document.createTextNode(commande.nom));
                nom.href = `/produit/detail/${commande.code}`;


                historiquePanier.ajouterLigne(table, image, nom, commande.prix, commande.nb);

                historiquePanier["div-historique"].appendChild(table);

                var date_commande = document.createElement('h6');
                date_commande.style.margin = "40px";
                date_commande.style.color = 'green';
                var d = historiquePanier.panier.date[i];

                date_commande.appendChild(document.createTextNode(`Commande passée le ${(new Date(d)).toLocaleDateString()}`))
                historiquePanier["div-historique"].insertBefore(date_commande, table);

            }
        } else {
            var texte = document.createElement('h4');
            texte.appendChild(document.createTextNode("Vous n'avez pas de commandes précédentes"));
            historiquePanier["div-historique"].appendChild(texte)

        }

    },


    "ajouterLigne": function (table, img, nom, prix, nb, typeCellule = 'td') {

        var ligne = document.createElement('tr');

        historiquePanier.ajouterCell(ligne, img, typeCellule);
        historiquePanier.ajouterCell(ligne, nom, typeCellule);
        historiquePanier.ajouterCell(ligne, prix, typeCellule);

        historiquePanier.ajouterCell(ligne, nb, typeCellule);

        table.appendChild(ligne);


    },

    "ajouterCell": function (ligne, contenuCellule, typeCellule = 'td') {

        var cell = document.createElement(typeCellule);
        cell.append(contenuCellule);
        ligne.appendChild(cell);

    },


    "initialisation": function () {

        historiquePanier["div-historique"] = document.getElementById('div-historique');

        historiquePanier.historique();

    },


}


window.addEventListener("load", historiquePanier.initialisation, false);