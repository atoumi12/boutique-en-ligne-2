from flask import Flask, session, g
import database as db
import sqlAlchemy as sqlA

from web.routes import site
from produit.routes import produit
from ligneProduit.routes import ligne_produit
from auth.routes import auth
from errors.routes import errors
from panier.routes import panier
from api.routes import un_api

app = Flask(__name__)
app.secret_key = 'f9bf78b9a18ce6d46a0cd2b0b86df9da'


@app.before_request
def before_request():
    db.get_connection()
    sqlA.alchemy_connection()

    if session.get('username') and session.get('info'):
        user = {"name": session["username"], "info": session["info"]}
    else:
        user = {"name": "Guest", "info": None}
    g.user = user

    if session.get('messageConnexion'):
        messageConnexion = session.get('messageConnexion')
    else:
        messageConnexion = ""
    g.mConnexion = messageConnexion
    session.pop('messageConnexion', None)

    if session.get('messageCreationCompte'):
        messageCreation = session.get('messageCreationCompte')
    else:
        messageCreation = ""
    g.mCreation = messageCreation
    session.pop('messageCreationCompte', None)

    if session.get('m-disconnect'):
        messageDeconnection = session['m-disconnect']
    else:
        messageDeconnection = ""

    g.mDisconnect = messageDeconnection
    session.pop('m-disconnect', None)


@app.after_request
def after_request(response):
    sqlA.alchemy_disconnect(response)
    return db.close_connection(response)


app.register_blueprint(site)
app.register_blueprint(produit)
app.register_blueprint(ligne_produit)
app.register_blueprint(auth)
app.register_blueprint(errors)
app.register_blueprint(panier)

app.register_blueprint(un_api)

if __name__ == "__main__":
    app.run(debug=True)
