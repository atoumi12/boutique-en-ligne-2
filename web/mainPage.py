from flask import g


class HomePage:

    def prods(self):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)

            requete = f"""Select *
                            from classicmodels.products order by quantityInStock desc limit 5;"""

            cursor.execute(requete)

            # get all records
            records = cursor.fetchall()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def prodsCarousel(self):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)

            requete = f"""Select *
                            from classicmodels.products order by quantityInStock desc limit 10;"""

            cursor.execute(requete)

            # get all records
            records = cursor.fetchall()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def prods_lines(self):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)

            requete = f"""Select *
                            from classicmodels.productlines;"""

            cursor.execute(requete)

            # get all records
            records = cursor.fetchall()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)
