from flask import Blueprint, render_template,session,g
from web import mainPage
from api import methods


site = Blueprint('site', __name__, template_folder='templates')

main_page = mainPage.HomePage()
c= methods.Methods()


@site.route('/')
def index():
    prods = main_page.prods()
    ligne_produits = main_page.prods_lines()
    # importation via SQLAlchemy :)
    carousel = c.carousel_items()

    m = ""
    if session.get('messageSuppProd'):
        m = session['messageSuppProd']
        session.pop('messageSuppProd', None)

    commande = ""
    if session.get("pay"):
        commande = session["pay"]
        session.pop("pay", None)

    return render_template('home.html', prods=prods, lignesProds=ligne_produits, carousel=carousel,
                           suppProd=m, messageConnexion=g.mConnexion, commande=commande,
                           messageCreation=g.mCreation, mDisconnect =g.mDisconnect, user=g.user)

