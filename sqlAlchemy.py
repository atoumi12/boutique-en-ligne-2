from flask import g

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, String

from sqlalchemy import select


Base = declarative_base()


class Produit(Base):
    __tablename__ = 'products'

    # Colonne clé primaire
    productCode = Column(String, primary_key=True)
    # Champs obligatoire
    productName = Column(String)
    productLine = Column(String)
    productScale = Column(String)
    productVendor = Column(String)
    productDescription = Column(String)
    quantityInStock = Column(Integer)
    buyPrice = Column(Integer)
    MSRP = Column(Integer)

    def __repr__(self):
        return "'productName':'%s', 'productLine': '%s' , 'MSRP': '%s', 'productCode':'%s', 'productScale': '%s'," \
               " 'productVendor':'%s', 'productDescription':'%s', 'quantityInStock':'%s'" \
               " 'buyPrice':'%s' " % (
            self.productName, self.productLine, self.MSRP, self.productCode, self.productScale,
            self.productVendor, self.productDescription, self.quantityInStock, self.buyPrice)

    # def as_dict(self):
    #     return {c.productName: getattr(self, c.productName) for c in self.__table__.columns}


def alchemy_connection():
    engine = create_engine('mysql+mysqldb://root:@localhost/classicmodels')
    Session = sessionmaker()
    Session.configure(bind=engine)
    g.session = Session()


def alchemy_disconnect(response):
    if g.session is not None:
        print("Closing SqlALchemy connection")
        g.session.close()
    return response


