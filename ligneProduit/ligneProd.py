from flask import g


class LigneProduit:

    def valider_ligne_produit(self, request):
        errors = {}

        if "productLine" not in request.form:
            errors['productCode'] = "Pas de ligne de produit."

        if "textDescription" in request.form:
            if len(request.form['textDescription']) < 3:
                errors['textDescription'] = "La description de la ligne produit doit avoir au moins 3 caractères"

            if len(request.form['textDescription']) > 200:
                errors['textDescription'] = "La description de la ligne de produit ne doit pas avoir plus de 300 caractères"
        else:
            errors['textDescription'] = "Pas de description de produit"

        if len(list(errors.keys())) > 0:
            return errors
        else:
            return False

    def one_cat(self, productLine):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)

            requete = f""" select * from productlines 
                            where productLine = %s ; """

            params = (productLine,)
            cursor.execute(requete, params)

            # get all records
            records = cursor.fetchall()
            return records[0]

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def afficher_ligne_produit(self, productLine):
        try:
            connection = g.connection
            cursor = connection.cursor(dictionary=True)
            requete = f"""select * from products 
                            where productLine = %s ;"""

            params = (productLine,)
            cursor.execute(requete, params)
            # get all records
            records = cursor.fetchall()
            return records

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def update_ligne(self, formulaire, ligne):

        ##validation
        errors = self.valider_ligne_produit(formulaire)
        if errors:
            return errors
        else:
            try:
                connection = g.connection
                cursor = connection.cursor(dictionary=True)

                requete = f"""update productlines
                            set productLine = %s,
                                textDescription = %s
                            where productLine = %s ;
                """
                params = (formulaire.form['productLine'], formulaire.form['textDescription'].strip(),
                         ligne,)

                cursor.execute(requete, params)
                # commit
                connection.commit()
                return False
            except Exception as e:
                print("Error reading data from MySQL table", e)

    def add_ligne_produit(self, formulaire):
        ##validation
        errors = self.valider_ligne_produit(formulaire)
        if errors:
            return errors
        else:
            try:
                connection = g.connection
                cursor = connection.cursor(dictionary=True)

                requete = f""" insert into productlines
                               (  productLine,
                                  textDescription )
                               values (%s, %s);
                   """
                params = (formulaire.form['productLine'], formulaire.form['textDescription'].strip(),)
                cursor.execute(requete, params)
                new_productLine = formulaire.form['productLine']

                # commit
                connection.commit()
                return new_productLine

            except Exception as e:
                print("Error reading data from MySQL table", e)
                return False

    def ligne_vide(self):
        return {
            'productLine': "",
            'textDescription': ""
        }
