from flask import Blueprint,request, render_template,redirect,url_for,g
from  ligneProduit import ligneProd

ligne_produit = Blueprint('ligne_produit', __name__, template_folder='templates', url_prefix='/ligneProduit')

lignes = ligneProd.LigneProduit()


@ligne_produit.route('/detail/<productLine>')
def index_ligne(productLine):
    cat = lignes.one_cat(productLine)
    all = lignes.afficher_ligne_produit(productLine)
    return render_template('detailLigneProduit.html', cat=cat, all=all, user=g.user)


## Modification d'une ligne de produit
@ligne_produit.route('/edit/<productLine>', methods=['POST', 'GET'])
def edit_ligneProduit(productLine):
    errors = False
    ok = False
    if request.method == "POST":
        errors = lignes.update_ligne(request,productLine)
        if not errors :
            ok = True

    return render_template("actionLigneProduit.html", user=g.user,ligne=lignes.one_cat(productLine), errors=errors, ok=ok)



## Ajout nouvelle ligne de produit
@ligne_produit.route('/add', methods=['POST', 'GET'])
def add_ligne():
    if request.method == "POST":
        new_productLine = lignes.add_ligne_produit(request)
        if not new_productLine:
            return "Une erreur s'est produite."

        return redirect(url_for('ligne_produit.index_ligne', productLine=new_productLine))

    return render_template("actionLigneProduit.html", user=g.user, ligne=lignes.ligne_vide(), errors=False, ok=False)





