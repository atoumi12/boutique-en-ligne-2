import simplejson as json

from flask import Blueprint, request, make_response, jsonify, session
from api import methods

import stripe

un_api = Blueprint('un_api', __name__, url_prefix='/api', static_folder='static', static_url_path='staticApi')

stripe_keys = {
    "secret_key": "sk_test_51IotySARKuYhkC317Gom0V8PMoMr11Jpqitg1LgJ45WVVpddn7JQldQtSosfFuS4jfcO7te7rD1nMFsvqrbdIiE8002xRD0c0u",
    "publishable_key": "pk_test_51IotySARKuYhkC31q1W5Veic4tztIVej4h7M3mhc3kAgut6oUP9oNRPRjHIE1989Ba3q2WJoZi4Q8YSih9qGumCc00gzYYwAgi",
}

stripe.api_key = stripe_keys["secret_key"]

m = methods.Methods()


@un_api.route('/')
def index():
    return {'ping': 'API OK!'}


@un_api.route('/recherche')
def recherche():
    key_word = request.args.get('search', default='', type=str)

    if len(key_word) == 0:
        return recherche_erreur()

    resultat_recherche = m.recherche_produit(key_word)

    if len(resultat_recherche) == 0:
        return recherche_erreur()

    r = make_response(json.dumps(
        {
            'data': resultat_recherche
        }
    ))

    r.mimetype = 'application/json'

    return r


def recherche_erreur():
    r = make_response(json.dumps({"error": "Pas de correspondance trouvée."}))
    r.mimetype = 'application/json'
    return r


# stripe
@un_api.route("/config")
def get_publishable_key():
    stripe_config = {"publicKey": stripe_keys["publishable_key"]}
    return jsonify(stripe_config)


@un_api.route("/create-checkout-session", methods=['POST'])
def create_checkout_session():
    somme = request.json['somme']
    somme = float(somme) * 100
    somme = int(somme)

    domain_url = "http://localhost:5000/"
    try:
        checkout_session = stripe.checkout.Session.create(
            payment_method_types=["card"],
            mode="payment",
            line_items=[
                {
                    'price_data': {
                        'currency': 'cad',
                        'unit_amount': somme,
                        'product_data': {
                            'name': 'Total',
                        },
                    },
                    'quantity': 1,
                },
            ],
            success_url=domain_url,
            cancel_url=domain_url,
        )

        # permet d'ajouter un peut message de confirmation au user
        session["pay"] = "Votre commande a été passée avec succès!"

        return jsonify({"sessionId": checkout_session["id"]})
    except Exception as e:
        return jsonify(error=str(e)), 403
