from flask import g
from sqlalchemy import select
from sqlAlchemy import Produit
from sqlalchemy import or_, desc


class Methods:

    def recherche_produit(self, keyword=''):
        try:
            session = g.session

            search = "%{}%".format(keyword)
            statement = select(Produit).where(
                or_(Produit.productName.ilike(search), Produit.productDescription.ilike(search))
            )

            rec = []
            for row in session.execute(statement):
                rec.append(self.conv(row))

            return rec

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def carousel_items(self):
        try:
            session = g.session

            statement = select([Produit]).order_by(
                desc(Produit.quantityInStock)
            ).limit(10)

            rec = []
            for row in session.execute(statement):
                rec.append(self.conv(row))

            return rec

        except Exception as e:
            print("Error reading data from MySQL table", e)

    def conv(self, row):
        return {
            "productName": row[0].productName,
            "productLine": row[0].productLine,
            "MSRP": row[0].MSRP,
            "productCode": row[0].productCode,
            "productScale": row[0].productScale,
            "productVendor": row[0].productVendor,
            "productDescription": row[0].productDescription,
            "quantityInStock": row[0].quantityInStock
        }
