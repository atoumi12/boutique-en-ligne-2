'use strict'

$(document).ready(function () {

        function paiement() {

            fetch("/api/config")
                .then((result) => {
                    return result.json();
                })
                .then((data) => {
                    // variable stripe avec la cle obtenue dans la route /api/config
                    const stripe = Stripe(data.publicKey);

                    document.querySelector("#btn-commande").addEventListener("click", () => {

                        // disable le bouton apres un click de paiement
                        $('#btn-commande').prop("disabled", true)

                        var sommeT = document.getElementById("total-commande").textContent
                        var somme = sommeT.trim().substr(0, sommeT.length - 2)
                        console.log(somme)

                        // Get Checkout Session ID

                        fetch("/api/create-checkout-session",
                            {
                                method: "POST",
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify(
                                    {somme: somme}
                                )
                            })

                            .then((result) => {
                                return result.json();
                            })
                            .then(function (session) {

                                // vider le panier de l'utilisateur
                                var user = document.cookie.split('=')[1]
                                localStorage.removeItem(user)

                                // Redirect to Stripe Checkout
                                return stripe.redirectToCheckout({sessionId: session.sessionId});
                            })
                            .then((res) => {
                                console.log(res);

                            });
                    });


                });
        }

        // un setTimeOut parce que ce fichier s'excute plus rapidement que le "scriptVoirMonPanier.js"
        // Du coup accéder à des propriétés modifiées dans ce dernier est impossible ;)
        setTimeout(paiement, 2000)

    }
)
