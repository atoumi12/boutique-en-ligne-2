'use strict';

var recherche = {

    $: function () {

        $('#keyword').keyup(function () {
                $('#affichage-recherche').empty()
                var mot = $('#keyword').val();


                $.get(
                    "/api/recherche",
                    {search: mot},
                    function (data) {

                        if (mot.length > 0) {

                            // Gerer le click en dehors de la barre de recherche
                            setTimeout(function () {
                                $(document).click(gererClickEnDehorsZone);
                            }, 1000);


                            $('#affichage-recherche').removeClass('cacher')

                            if (data.error) {
                                $("#affichage-recherche").text(data.error);
                            } else {

                                data = data['data']

                                // data = JSON.parse(data)

                                for (var i = 0; i < 5; i++) {

                                    var row = [];
                                    var space = "style = \"margin: 15px\""
                                    var info = ""
                                    info += "<img width='50' src='https://loremflickr.com/300/300/ " + data[i].productLine + "'></img>"
                                    info += "<a " + space + "href=/produit/detail/" + data[i].productCode + " >" + data[i].productName + "</a>"
                                    info += "<a " + space + "   href=/ligneProduit/detail/" + data[i].productLine + " >" + data[i].productLine + "</a>"

                                    info += "</br>"
                                    row.push(info)

                                    $("#affichage-recherche").append(
                                        "" + row.join("") + ""
                                    );

                                }
                            }
                        } else {
                            $('#affichage-recherche').addClass('cacher')
                        }


                    }
                )

            }
        );


        function gererClickEnDehorsZone(evenement) {

            var clicDedans = document.getElementById("affichage-recherche").contains(evenement.target);

            if (!clicDedans) {
                $('#affichage-recherche').addClass('cacher');
                document.removeEventListener('click', gererClickEnDehorsZone);
            }
        };


    }

}


// Initialiser le tout
recherche.$();

