from flask import Blueprint,render_template,g,request,redirect

panier = Blueprint('panier', __name__, template_folder='templates', url_prefix='/panier')


@panier.route('/')
def index():
    if request.cookies.get('username') is None:
        return redirect('/auth/loginForm')

    return render_template('panier.html', user=g.user)


@panier.route('/historique')
def historique():
    return render_template("historiquePanier.html", user=g.user)

